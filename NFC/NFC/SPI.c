/*
 * SPI.c
 *
 * Created: 28/10/2014 18:28:34
 *  Author: Xander
 */ 

#include "SPI.h"

uint8_t SPI_SEND(uint8_t SPI_DATA){
	SPDR = SPI_DATA;											// send the data
	while(!(SPSR & (1 << SPIF)));								// wait until transmission is complete
	return SPDR;
}

void SPI_INIT(void){
	DDRB |= (1 << PB5) | (1 << PB3);							// SCK and MOSI as outputs
	DDRB &= ~(1 << 4);											// MISO as input

	SPCR |= (1 << MSTR);										// Set as Master
	SPCR &= ~(1 << SPR0);										// Divided clock by 32
	SPCR |= 1 << (1 << SPR1);
	SPSR |= (1 << SPI2X );
	SPCR &= ~(1 << CPOL) & ~(1 << CPHA);
	SPCR |= (1 << SPE);											// Enable SPI
}