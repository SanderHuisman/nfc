/*
 * SPI.h
 *
 * Created: 28/10/2014 18:28:56
 *  Author: Xander
 */ 

#define F_CPU 16000000UL

#include <avr/io.h>
#include <stdio.h>
#include <util/delay.h>
#include <avr/interrupt.h>

//#ifndef SPI_H_
//#define SPI_H_

uint8_t SPI_SEND(uint8_t SPI_DATA);
void SPI_INIT(void);

//#endif /* SPI_H_ */