/*
 * NFC.c
 *
 * Created: 29/10/2014 17:12:46
 *  Author: Xander
 */ 

#define F_CPU 16000000UL

#include <avr/io.h>
#include <stdio.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define BAUD_RATE 9600
#define BAUD_PRESCALLER (((F_CPU / (BAUD_RATE * 16UL))) - 1)


void USART_init(void);
void uart_flush(void);
int uart_putch(char ch, FILE *stream);	
int uart_getch(FILE *stream);	
void ansi_cl(void);
void ansi_me(void);
FILE uart_str = FDEV_SETUP_STREAM(uart_putch, uart_getch, _FDEV_SETUP_RW);

#include "MFRC522.h"

void USART_init(void){
	stdout = stdin = &uart_str;
	
	UBRR0H = (BAUD_PRESCALLER >> 8);								// High byte van de baud prescaler
	UBRR0L = BAUD_PRESCALLER;										// Low byte van de baud prescaler
	UCSR0B |= (1 << RXEN0) | (1 << TXEN0);							// Zet de ontvanger en zender aan
	//UCSR0B |= (1 << RXCIE0);										// Zet de interrupt van de ontvanger aan
	UCSR0C |= (1 << UCSZ01) | (1 << UCSZ00);
	
	// Clear Screen
	ansi_me();
	ansi_cl();
	ansi_me();
	ansi_cl();
	uart_flush();
}

void uart_flush(void){
	unsigned char dummy;
	while(UCSR0A & (1 << RXC0)) 
		dummy = UDR0;
}

int uart_putch(char ch, FILE *stream){
	if(ch == '\n')
		uart_putch('\r', stream);
	while(!(UCSR0A & (1 << UDRE0)));
	UDR0 = ch;
	return 0;
}
	
int uart_getch(FILE *stream){
	unsigned char ch;
	while(!(UCSR0A & (1 << RXC0)));
	ch = UDR0;

	uart_putch(ch, stream);								/* Echo the Output Back to terminal */

	return ch;
}
	
void ansi_cl(void){
	// ANSI clear screen: cl=\E[H\E[J
	putchar(27);
	putchar('[');
	putchar('H');
	putchar(27);
	putchar('[');
	putchar('J');
}

void ansi_me(void){
	// ANSI turn off all attribute: me=\E[0m
	putchar(27);
	putchar('[');
	putchar('0');
	putchar('m');
}

int main(void){	
	USART_init();	
	
	MFRC522();
	PCD_Init();
	
    while(1){
		if (!PICC_IsNewCardPresent()){
			printf("1\n");
			//return;
		}
		else{

			// Select one of the cards
			if(!PICC_ReadCardSerial()){
				printf("2\n");
				//return;

				// Dump debug info about the card. PICC_HaltA() is automatically called.
				printf("readserial\n");
			}
			else
				PICC_DumpToSerial(&uid);
		}
    }
}